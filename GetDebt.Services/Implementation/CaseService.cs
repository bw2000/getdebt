﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.DataAccess.Repositories;
using GetDebt.Model.Case;
using System.Collections.Generic;

namespace GetDebt.Services.Implementation
{
    public class CaseService : ICaseService
    {
        private readonly ICaseRepository _caseRepository;
        private readonly IActionRepository _actionRepository;
        private readonly ICaseStageRepository _caseStageRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CaseService(
            ICaseRepository sprawaRepository,
            ICaseStageRepository sprawaEtapRepository,
            IActionRepository akcjaRepository,
            IUnitOfWork unitOfWork)
        {
            _caseRepository = sprawaRepository;
            _caseStageRepository = sprawaEtapRepository;
            _actionRepository = akcjaRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<CaseMainModel> GetCases()
        {
            var cases = _caseRepository.GetAll(s => s.Balance, s => s.CaseStage);
            return cases;
        }

        public IEnumerable<CaseStageModel> GetAllCaseStages()
        {
            var caseStages = _caseStageRepository.GetAll();
            return caseStages;
        }

        public CaseMainModel GetCase(int caseId)
        {
            var casebyId = _caseRepository.GetById(caseId);
            return casebyId;
        }

        public void CreateCase(CaseMainModel caseModel)
        {
            _caseRepository.Add(caseModel);
        }

        public void SaveCase()
        {
            _unitOfWork.Commit();
        }
    }
}

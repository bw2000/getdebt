﻿using GetDebt.Model.Case;
using System.Collections.Generic;

namespace GetDebt.Services
{
    public interface ICaseService
    {
        IEnumerable<CaseMainModel> GetCases();
        IEnumerable<CaseStageModel> GetAllCaseStages();
        CaseMainModel GetCase(int id);
        void CreateCase(CaseMainModel caseModel);
        void SaveCase();
    }
}

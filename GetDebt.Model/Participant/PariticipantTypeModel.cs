﻿namespace GetDebt.Model.Participant
{
    public class ParticipantTypeModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
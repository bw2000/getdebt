﻿using System;

namespace GetDebt.Model.Participant
{
    public class ParticipantModel
    {
        public int Id { get; set; }
        public int Synonym { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string PESEL { get; set; }
        public string NIP { get; set; }
        public string REGON { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string IdentificationDokument { get; set; }

        public int ParticipantTypeID { get; set; }

        public ParticipantTypeModel ParticipantType { get; set; }
        //public virtual List<AdresModel> Adres { get; set; }
    }
}

﻿using GetDebt.Model.Case;
using GetDebt.Model.Participant;

namespace GetDebt.Model.Debtor
{
    public class DebtorModel
    {
        public int Id { get; set; }

        public int CaseID { get; set; }
        public int ParticipantID { get; set; }

        public virtual CaseMainModel CaseMain { get; set; }
        public virtual ParticipantModel Participant { get; set; }
    }
}

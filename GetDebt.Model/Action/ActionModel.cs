﻿using GetDebt.Model.Case;
using System;

namespace GetDebt.Model.Action
{
    public class ActionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime CreationDate { get; set; }
        
        public int CaseID { get; set; }
        public int AkcjaTypID { get; set; }

        public virtual CaseMainModel CaseMain { get; set; }
        public virtual ActionTypeModel ActionType { get; set; }

    }
}

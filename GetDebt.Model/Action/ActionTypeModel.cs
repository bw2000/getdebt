﻿namespace GetDebt.Model.Action
{
    public class ActionTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
﻿using System;

namespace GetDebt.Model.Balance
{
    public class BalanceModel
    {
        public int Id { get; set; }
        public decimal Receivables { get; set; } // Należności
        public decimal Interest { get; set; } // Odsetki
        public decimal Remission { get; set; } // Umorzenie
        public decimal Paid { get; set; } // Zapłacono
        public decimal Expences { get; set; }
        public decimal Due { get; set; } // Do zapłaty
        public DateTime BalanceDate { get; set; }

        public BalanceModel()
        {
            BalanceDate = DateTime.Now;
        }
    }
}

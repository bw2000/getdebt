﻿namespace GetDebt.Model.Case
{
    public class CaseStageModel
    {
        public int Id { get; set; }
        public string Stage { get; set; }
    }
}

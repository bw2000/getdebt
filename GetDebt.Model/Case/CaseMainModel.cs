﻿using GetDebt.Model.Action;
using GetDebt.Model.Balance;
using System;
using System.Collections.Generic;

namespace GetDebt.Model.Case
{
    public class CaseMainModel
    {
        public int Id { get; set; }
        public int CaseNumber { get; set; }
        public int CaseYear { get; set; }
        public string Sign { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ClosingDate { get; set; }

        public int BalanceID { get; set; }
        public int CaseStageID { get; set; }

        public virtual BalanceModel Balance { get; set; }
        public virtual CaseStageModel CaseStage { get; set; }
        //public virtual List<ActionModel> Actions { get; set; }

        public CaseMainModel()
        {
            CreationDate = DateTime.Now;
            CaseYear = DateTime.Now.Year;
        }
    }
}

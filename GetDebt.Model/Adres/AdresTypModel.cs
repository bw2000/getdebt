﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetDebt.Model.Adres
{
    public class AdresTypModel
    {
        public int Id { get; set; }
        public string Typ { get; set; }
        
        public AdresModel Adres { get; set; }
    }
}

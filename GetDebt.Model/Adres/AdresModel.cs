﻿using GetDebt.Model.Participant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetDebt.Model.Adres
{
    public class AdresModel
    {
        public int Id { get; set; }
        public string Ulica { get; set; }
        public string NrDomu { get; set; }
        public string NrMieszkania { get; set; }
        public string KodPocztowy { get; set; }
        public string Miasto { get; set; }
        public string Poczta { get; set; }
        public string Powiat { get; set; }
        public string Wojewodztwo { get; set; }
        public string Kraj { get; set; }
        public string Komentarz { get; set; }
        public DateTime DataDodania { get; set; }
        public bool Aktywny { get; set; }

        public int UczestnikID { get; set; }
        public int AdresTypID { get; set; }

        public ParticipantModel Uczestnik { get; set; }
        public AdresTypModel AdresTyp { get; set; }
    }
}

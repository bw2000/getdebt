﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.Model.Action;

namespace GetDebt.DataAccess.Repositories
{
    public interface IActionRepository : IRepository<ActionModel>
    {

    }
}

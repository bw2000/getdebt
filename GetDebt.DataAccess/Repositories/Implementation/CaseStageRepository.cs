﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.DataAccess.Infrastructure.Implemenation;
using GetDebt.Model.Case;

namespace GetDebt.DataAccess.Repositories.Implementation
{
    public class CaseStageRepository : RepositoryBase<CaseStageModel>, ICaseStageRepository
    {
        public CaseStageRepository(IDbFactory dbFactory) : base(dbFactory) { }
    }
}

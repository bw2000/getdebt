﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.DataAccess.Infrastructure.Implemenation;
using GetDebt.Model.Action;

namespace GetDebt.DataAccess.Repositories.Implementation
{
    public class ActionRepository : RepositoryBase<ActionModel>, IActionRepository
    {
        public ActionRepository(IDbFactory dbFactory) : base(dbFactory) { }
    }
}

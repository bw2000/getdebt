﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.DataAccess.Infrastructure.Implemenation;
using GetDebt.Model.Case;
using System.Linq;

namespace GetDebt.DataAccess.Repositories.Implementation
{
    public class CaseRepository : RepositoryBase<CaseMainModel>, ICaseRepository
    {
        public CaseRepository(IDbFactory dbFactory) : base(dbFactory) { }

        public CaseMainModel GetCaseById(int caseId)
        {
            var caseMain = this.DbContext.CaseMain.Where(s => s.Id == caseId).FirstOrDefault();

            return caseMain;
        }

        public CaseMainModel GetCaseByNumber(int caseNumber, int caseYear)
        {
            var caseMain = this.DbContext.CaseMain.Where(   
                s => s.CaseNumber == caseNumber && s.CaseYear == caseYear).FirstOrDefault();

            return caseMain;
        }
    }
}

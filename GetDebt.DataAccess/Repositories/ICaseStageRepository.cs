﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.Model.Case;

namespace GetDebt.DataAccess.Repositories
{
    public interface ICaseStageRepository : IRepository<CaseStageModel>
    {

    }
}

﻿using GetDebt.DataAccess.Infrastructure;
using GetDebt.Model.Case;

namespace GetDebt.DataAccess.Repositories
{
    public interface ICaseRepository : IRepository<CaseMainModel>
    {
        CaseMainModel GetCaseById(int id);
        CaseMainModel GetCaseByNumber(int nrSprawy, int rokSprawy);
    }
}

﻿using System;

namespace GetDebt.DataAccess.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        GetDebtEntities Init();
    }
}

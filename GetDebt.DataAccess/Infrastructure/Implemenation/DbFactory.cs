﻿using GetDebt.DataAccess.Infrastructure.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetDebt.DataAccess.Infrastructure.Implemenation
{
    public class DbFactory : Disposable, IDbFactory
    {
        GetDebtEntities dbContext;

        public GetDebtEntities Init()
        {
            return dbContext ?? (dbContext = new GetDebtEntities());
        }

        protected override void DisposeCore()
        {
            if(dbContext != null)
                dbContext.Dispose();
        }
    }
}

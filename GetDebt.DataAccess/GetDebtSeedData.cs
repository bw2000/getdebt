﻿using GetDebt.Model.Action;
using GetDebt.Model.Balance;
using GetDebt.Model.Case;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace GetDebt.DataAccess
{
    public class GetDebtSeedData : DropCreateDatabaseAlways<GetDebtEntities>
    {
        List<CaseStageModel> lista = GetCaseStages();
        protected override void Seed(GetDebtEntities context)
        {
            GetCaseStages().ForEach(a => context.CaseStage.Add(a));
            GetBalance().ForEach(a => context.Balance.Add(a));
            context.Commit();

            GetCase().ForEach(s => context.CaseMain.Add(s));
            context.Commit();
        }
        private static List<CaseStageModel> GetCaseStages()
        {
            return new List<CaseStageModel>
            {
                new CaseStageModel
                {
                    Stage = "Sądowy"
                },
                new CaseStageModel
                {
                    Stage = "Polubowny"
                },
                new CaseStageModel
                {
                    Stage = "Egzekucyjny"
                }
            };
        }

        private static List<BalanceModel> GetBalance()
        {
            return new List<BalanceModel>
            {
                new BalanceModel
                {
                    Due = 500,
                    Expences = 100,
                    Interest = 0,
                    Paid = 122,
                    Receivables = 62,
                    Remission = 342,
                }
            };
        }
        private static List<ActionModel> GetAction()
        {
            return new List<ActionModel>
            {
                new ActionModel
                {
                   CaseID = 1, 
                   Name = "Dodanie sprawy do systemu",
                   Code = "START",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 1,
                   Name = "Przekazanie sprawy do terenu",
                   Code = "TEREN",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 2,
                   Name = "Dodanie sprawy do systemu",
                   Code = "START",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 2,
                   Name = "Przekazanie sprawy do terenu",
                   Code = "TEREN",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 3,
                   Name = "Dodanie sprawy do systemu",
                   Code = "START",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 3,
                   Name = "Przekazanie sprawy do terenu",
                   Code = "TEREN",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 4,
                   Name = "Dodanie sprawy do systemu",
                   Code = "START",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 4,
                   Name = "Przekazanie sprawy do terenu",
                   Code = "TEREN",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 5,
                   Name = "Dodanie sprawy do systemu",
                   Code = "START",
                   CreationDate = DateTime.Now
                },
                new ActionModel
                {
                   CaseID = 5,
                   Name = "Przekazanie sprawy do terenu",
                   Code = "TEREN",
                   CreationDate = DateTime.Now
                },
            };
        }

        private static List<CaseMainModel> GetCase()
        {
            return new List<CaseMainModel>
            {
                new CaseMainModel
                {
                    CaseNumber = 1,
                    CaseStageID = 1,
                    BalanceID = 1,
                    CaseYear = DateTime.Now.Year,
                    CreationDate = DateTime.Now,
                    ClosingDate = null,
                    Sign = "Test1"
                },
                new CaseMainModel
                {
                    CaseNumber = 2,
                    CaseStageID = 1,
                    BalanceID = 1,
                    CaseYear = DateTime.Now.Year,
                    CreationDate = DateTime.Now,
                    ClosingDate = null,
                    Sign = "Test1"
                },
                new CaseMainModel
                {
                    CaseNumber = 3,
                    CaseStageID = 1,
                    BalanceID = 1,
                    CaseYear = DateTime.Now.Year,
                    CreationDate = DateTime.Now,
                    ClosingDate = null,
                    Sign = "Test1"
                },
                new CaseMainModel
                {
                    CaseNumber = 4,
                    CaseStageID = 1,
                    BalanceID = 1,
                    CaseYear = DateTime.Now.Year,
                    CreationDate = DateTime.Now,
                    ClosingDate = null,
                    Sign = "Test1"
                },
                new CaseMainModel
                {
                    CaseNumber = 5,
                    CaseStageID = 1,
                    BalanceID = 1,
                    CaseYear = DateTime.Now.Year,
                    CreationDate = DateTime.Now,
                    ClosingDate = null,
                    Sign = "Test1"
                },
                new CaseMainModel
                {
                    CaseNumber = 6,
                    CaseStageID = 1,
                    BalanceID = 1,
                    CaseYear = DateTime.Now.Year,
                    CreationDate = DateTime.Now,
                    ClosingDate = null,
                    Sign = "Test1"
                }
            };
        }
    }
}

﻿using GetDebt.Model.Action;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class ActionConfiguration : EntityTypeConfiguration<ActionModel>
    {
        public ActionConfiguration()
        {
            ToTable("Action");
            Property(a => a.Name).IsRequired().HasMaxLength(100);
            Property(a => a.Code).IsRequired().HasMaxLength(10);
        }
    }
}

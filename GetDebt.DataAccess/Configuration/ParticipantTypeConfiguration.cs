﻿using GetDebt.Model.Participant;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class ParticipantTypeConfiguration : EntityTypeConfiguration<ParticipantTypeModel>
    {
        public ParticipantTypeConfiguration()
        {
            ToTable("ParticipantType");
            Property(u => u.Type).IsRequired().HasMaxLength(30);
        }
    }
}

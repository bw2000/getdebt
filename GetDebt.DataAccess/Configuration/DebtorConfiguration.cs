﻿using GetDebt.Model.Debtor;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class DebtorConfiguration : EntityTypeConfiguration<DebtorModel>
    {
        public DebtorConfiguration()
        {
            ToTable("Debtor");
            Property(d => d.CaseID).IsRequired();
            Property(d => d.ParticipantID).IsRequired();
        }
    }
}

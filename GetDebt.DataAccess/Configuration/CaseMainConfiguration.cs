﻿using GetDebt.Model.Case;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class CaseMainConfiguration : EntityTypeConfiguration<CaseMainModel>
    {
        public CaseMainConfiguration()
        {

            ToTable("CaseMain");
            Property(s => s.CaseNumber).IsRequired();
            Property(s => s.CaseYear).IsRequired();
            Property(s => s.CreationDate).IsRequired().HasColumnType("date");
        }
    }
}

﻿using GetDebt.Model.Balance;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class BalanceConfiguration : EntityTypeConfiguration<BalanceModel>
    {
        public BalanceConfiguration()
        {
            ToTable("Balance");
            Property(s => s.Due).IsRequired().HasPrecision(10, 2);
            Property(s => s.Expences).IsRequired().HasPrecision(10, 2);
            Property(s => s.Receivables).IsRequired().HasPrecision(10, 2);
            Property(s => s.Interest).IsRequired().HasPrecision(10, 2);
            Property(s => s.Remission).IsRequired().HasPrecision(10, 2);
            Property(s => s.Paid).IsRequired().HasPrecision(10, 2);
            Property(s => s.BalanceDate).IsRequired();

        }
    }
}

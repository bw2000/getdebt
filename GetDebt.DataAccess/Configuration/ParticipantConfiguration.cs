﻿using GetDebt.Model.Participant;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class ParticipantConfiguration : EntityTypeConfiguration<ParticipantModel>
    {
        public ParticipantConfiguration()
        {
            ToTable("Participant");
            Property(u => u.FirstName).IsRequired().HasMaxLength(30);
            Property(u => u.LastName).IsRequired().HasMaxLength(40);
        }
    }
}

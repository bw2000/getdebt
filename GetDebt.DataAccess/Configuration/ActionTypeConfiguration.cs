﻿using GetDebt.Model.Action;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class ActionTypeConfiguration : EntityTypeConfiguration<ActionTypeModel>
    {
        public ActionTypeConfiguration()
        {
            ToTable("ActionType");
            Property(a => a.Name).IsRequired().HasMaxLength(100);
        }
            
    }
}

﻿using GetDebt.Model.Case;
using System.Data.Entity.ModelConfiguration;

namespace GetDebt.DataAccess.Configuration
{
    public class CaseStageConfiguration : EntityTypeConfiguration<CaseStageModel>
    {
        public CaseStageConfiguration()
        {
            ToTable("CaseStage");
            Property(a => a.Stage).IsRequired().HasMaxLength(100);
        }
    }
}

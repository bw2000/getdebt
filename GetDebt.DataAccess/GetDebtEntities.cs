﻿using GetDebt.DataAccess.Configuration;
using GetDebt.Model.Action;
using GetDebt.Model.Balance;
using GetDebt.Model.Case;
using GetDebt.Model.Debtor;
using GetDebt.Model.Participant;
using System.Data.Entity;

namespace GetDebt.DataAccess
{
    public class GetDebtEntities : DbContext
    {
        public GetDebtEntities() : base("GetDebtEntities") { }

        public DbSet<CaseMainModel> CaseMain { get; set; }
        public DbSet<ActionModel> Action { get; set; }
        public DbSet<ActionTypeModel> ActionType { get; set; }
        public DbSet<BalanceModel> Balance { get; set; }
        public DbSet<CaseStageModel> CaseStage { get; set; }
        public DbSet<ParticipantModel> Participant { get; set; }
        public DbSet<ParticipantTypeModel> ParticipantType { get; set; }
        public DbSet<DebtorModel> Debtor { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CaseMainConfiguration());
            modelBuilder.Configurations.Add(new CaseStageConfiguration());
            modelBuilder.Configurations.Add(new ActionConfiguration());
            modelBuilder.Configurations.Add(new ActionTypeConfiguration());
            modelBuilder.Configurations.Add(new BalanceConfiguration());
            modelBuilder.Configurations.Add(new ParticipantConfiguration());
            modelBuilder.Configurations.Add(new ParticipantTypeConfiguration());
            modelBuilder.Configurations.Add(new DebtorConfiguration());
        }
    }
}

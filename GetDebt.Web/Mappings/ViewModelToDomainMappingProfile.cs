﻿using AutoMapper;
using GetDebt.Model.Case;
using GetDebt.Web.ViewModel;

namespace GetDebt.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<CaseMainFormViewModel, CaseMainModel>()
                .ForMember(s => s.CaseNumber, map => map.MapFrom(vm => vm.CaseNumber))
                .ForMember(s => s.CaseYear, map => map.MapFrom(vm => vm.CaseYear))
                .ForMember(s => s.Sign, map => map.MapFrom(vm => vm.Sign))
                .ForMember(s => s.CreationDate, map => map.MapFrom(vm => vm.CreationDate))
                .ForMember(s => s.ClosingDate, map => map.MapFrom(vm => vm.ClosingDate))
                .ForMember(s => s.BalanceID, map => map.MapFrom(vm => vm.BalanceID))
                .ForMember(s => s.CaseStageID, map => map.MapFrom(vm => vm.CaseStageID));
        }
    }
}
﻿using AutoMapper;
using GetDebt.Model.Action;
using GetDebt.Model.Case;
using GetDebt.Web.ViewModel;

namespace GetDebt.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<CaseMainModel, CaseMainViewModel>();
            Mapper.CreateMap<ActionModel, ActionViewModel>();
        }
    }
}
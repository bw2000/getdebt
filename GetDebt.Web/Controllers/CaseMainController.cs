﻿using AutoMapper;
using GetDebt.Model.Case;
using GetDebt.Services;
using GetDebt.Web.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace GetDebt.Web.Controllers
{
    public class CaseMainController : Controller
    {
        private readonly ICaseService _caseService;
        //private readonly IModelValidation<CaseMainModel> _modelValidation;

        public CaseMainController(ICaseService sprawaService
            //,IModelValidation<CaseMainModel> modelValidation
            )
        {
            _caseService = sprawaService;
            //_modelValidation = modelValidation;
        }

        public ActionResult Index()
        {
            IEnumerable<CaseMainViewModel> viewModelCases;
            IEnumerable<CaseMainModel> cases;

            cases = _caseService.GetCases().ToList();
            viewModelCases = Mapper.Map<IEnumerable<CaseMainModel>, IEnumerable<CaseMainViewModel>>(cases);

            return View(viewModelCases);
        }

        public ActionResult Details(int id)
        {
            CaseMainViewModel viewModelCase;
            CaseMainModel caseModel;

            caseModel = _caseService.GetCase(id);
            viewModelCase = Mapper.Map<CaseMainModel, CaseMainViewModel>(caseModel);

            return View(viewModelCase);
        }

        public ActionResult Create()
        {
            var viewModelCase = new CaseMainViewModel();

            var caseStageList = _caseService.GetAllCaseStages().ToList();
            viewModelCase.CaseStagesAll = caseStageList;
            object json = Json(viewModelCase).Data;
            ViewBag.Json = json;
            return View(viewModelCase);
        }

        [HttpPost]
        public ActionResult Create(CaseMainFormViewModel newSprawa)
        {
            //_modelValidation.NazwaMetody(newSprawa);
            var newSprawaForm = new NewSprawaFormViewModel();




            if (ModelState.IsValid)
            {
                var sprawa = Mapper.Map<CaseMainFormViewModel, CaseMainModel>(newSprawa);

                _caseService.CreateCase(sprawa);
                _caseService.SaveCase();
            }
            return RedirectToAction("Index", "Case");
        }

        //[HttpPost]
        //[Route("")]
        ////public ActionResult Create(NewSprawaFormViewModel newSprawaFormViewModel)
        //{
        //    var sprawaTemp = newSprawaFormViewModel.
        //    var sprawa = Mapper.Map<newSprawaFormViewModel.Sprawa, CaseMainModel>(newSprawaFormViewModel);


        //    return RedirectToAction("Index", "Case");
        //}
    }
}
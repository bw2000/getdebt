﻿
using GetDebt.Model.Balance;
using GetDebt.Model.Case;

namespace GetDebt.Web.Controllers
{
    public class NewSprawaFormViewModel
    {
        public BalanceModel Balance { get; set; }
        public CaseMainModel Case { get; set; }
    }
}
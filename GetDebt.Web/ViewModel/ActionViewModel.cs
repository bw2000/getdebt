﻿using GetDebt.Model.Case;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GetDebt.Web.ViewModel
{
    public class ActionViewModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime CreationDate { get; set; }

        public int CaseID { get; set; }
        //public int AkcjaTypID { get; set; }

        public CaseMainModel CaseModel { get; set; }
        //public ActionTypeModel AkcjaTyp { get; set; }
    }
}
﻿using GetDebt.Model.Action;
using GetDebt.Model.Balance;
using GetDebt.Model.Case;
using System;
using System.Collections.Generic;

namespace GetDebt.Web.ViewModel
{
    public class CaseMainViewModel
    {
        public int Id { get; set; }
        public int CaseNumber { get; set; }
        public int CaseYear { get; set; }
        public string Sign { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ClosingDate { get; set; }

        public int BalanceID { get; set; }
        public BalanceModel Balance { get; set; }

        public int CaseStageID { get; set; }
        public CaseStageModel CaseStage { get; set; }

        public ICollection<CaseStageModel> CaseStagesAll { get; set; }
        public virtual ICollection<ActionModel> Actions { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GetDebt.Web.ViewModel
{
    public class CaseMainFormViewModel
    {
        public int CaseNumber { get; set; }
        public int CaseYear { get; set; }
        public string Sign { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ClosingDate { get; set; }
        public int BalanceID { get; set; }
        public int CaseStageID { get; set; }
    }
}